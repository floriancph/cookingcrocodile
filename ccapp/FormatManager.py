

def formattingTagsToDB(text):
    tag_list = text.replace(" ", "")
    tag_list = tag_list.split("#")
    tag_list_stripped_lower = []
    # strip & lower
    for tag in tag_list:
        if tag != "":
            tag_list_stripped_lower.append(tag.strip().lower())
    # Remove duplicates
    result = list(dict.fromkeys(tag_list_stripped_lower))
    return result

def formattingTagsFromDB(tag_list):
    for i in range(0, len(tag_list)):
        tag_list[i] = "#" + tag_list[i].capitalize()
    return tag_list