import random

def generateToken(token_length):
    letters_cap = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    letters_low = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    token = ""

    for _ in range(0, token_length):
        rand = random.randint(0,2)
        if rand == 0:
            token = token + random.choice(letters_cap)
        elif rand == 1:
            token = token + random.choice(letters_low)
        elif rand == 2:
            token = token + random.choice(numbers)
    
    return token