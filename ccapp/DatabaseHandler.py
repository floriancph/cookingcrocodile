from ccapp import db
from ccapp.models import RecipeAndTag, Recipe

# Tag to Database

def saveTagsToDatabase(tags, recipe_id):
    for tag in tags:
        if RecipeAndTag.query.filter_by(tag=tag, recipe_id=recipe_id).first() == None:
            entry = RecipeAndTag(recipe_id=recipe_id, tag=tag)
            db.session.add(entry)
            #entry2 = Recipe.query.filter_by(recipe_id=recipe_id).first()
            #if entry2.tags == None:
                #entry2.tags = tag
            #else:
                #entry2.tags = entry2.tags + "," + tag
    db.session.commit()

def getTagsFromDatabase(recipe_id):
    tags = RecipeAndTag.query.filter_by(recipe_id=recipe_id).all()
    tag_list = []
    if tags != None:
        for element in tags:
            tag_list.append(element.tag)
    
    return tag_list