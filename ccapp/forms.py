from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired
from ccapp.models import User

class LoginForm(FlaskForm):
    email = StringField('email', validators=[DataRequired()])
    token = StringField('token')
    submit = SubmitField('Absenden')

class UserPictureForm(FlaskForm):
    picture = FileField("- Bild -", validators=[DataRequired(), FileAllowed(['jpg', 'png', 'jpeg'])])
    speichern = SubmitField("Hochladen")

class CreateRecipeForm(FlaskForm):
    title = StringField("title", validators=[DataRequired()])
    zutaten = TextAreaField("zutaten")
    zubereitung = TextAreaField("zubereitung")
    tags = StringField("tags")
    link = StringField("link")
    picture = FileField("- Bild -", validators=[FileAllowed(['jpg', 'png', 'jpeg'])])

    speichern = SubmitField("Speichern")

class ChangeRecipePicForm(FlaskForm):
    picture = FileField("- Bild -", validators=[DataRequired(), FileAllowed(['jpg', 'png', 'jpeg'])])
    speichern = SubmitField("Hochladen")

class ChangeProfileName(FlaskForm):
    name = StringField("tags")

class FeedbackForm(FlaskForm):
    feedback_text = TextAreaField("feedbackfield")
    absenden = SubmitField("Absenden")

class SearchForm(FlaskForm):
    search_field = StringField('search')
    search_btn = SubmitField("Suchen")