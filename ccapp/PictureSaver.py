from ccapp.TokenGenerator import generateToken
import ccapp.CREDENTIALS as CREDENTIALS
from ccapp import application as app
from PIL import Image, ExifTags
import os
import boto3
from botocore.exceptions import NoCredentialsError
import logging

ACCESS_KEY = CREDENTIALS.AWS_Access_Key
SECRET_KEY = CREDENTIALS.AWS_Secret_Key

def upload_to_aws(local_file, bucket, s3_file):
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY)
    try:
        s3.upload_file(local_file, bucket, s3_file)
        session = boto3.Session(aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
        s3 = session.resource('s3')
        object_acl = s3.ObjectAcl(bucket, s3_file)
        response = object_acl.put(ACL='public-read') # make image publicly available for website 
        # https://stackoverflow.com/questions/41904806/how-to-upload-a-file-to-s3-and-make-it-public-using-boto3
        # https://stackoverflow.com/questions/45981950/how-to-specify-credentials-when-connecting-to-boto3-s3
        logging.info("Picture Upload Successful")
        return True
    except FileNotFoundError:
        logging.error("The file was not found")
        return False
    except NoCredentialsError:
        logging.error("Credentials not available")
        return False

def save_picture(form_picture, folder):
    random_token = generateToken(30)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_token + f_ext
    picture_path = os.path.join(app.root_path, "static", folder, picture_fn)

    # Image Resizing
    output_size1 = (1080, 1080)
    output_size2 = (800, 800)
    i = Image.open(form_picture)
    try:
        if hasattr(i, '_getexif'): # only present in JPEGs
            for orientation in ExifTags.TAGS.keys(): 
                if ExifTags.TAGS[orientation]=='Orientation':
                    break 
            e = i._getexif()       # returns None if no EXIF data
            if e is not None:
                exif=dict(e.items())
                orientation = exif[orientation] 

                if orientation == 3:   i = i.transpose(Image.ROTATE_180)
                elif orientation == 6: i = i.transpose(Image.ROTATE_270)
                elif orientation == 8: i = i.transpose(Image.ROTATE_90)
        logging.info("File transformation worked")
    except Exception as e:
        logging.warning(f"File transformation did not work, {e}")

    i.thumbnail(output_size1)
    width, height = i.size
    new_width = 800
    new_height = 800
    
    left = (width - new_width)/2
    top = (height - new_height)/2
    right = (width + new_width)/2
    bottom = (height + new_height)/2
    cropped = i.crop((left, top, right, bottom))
    cropped.thumbnail(output_size2)

    cropped.save(picture_path)
    upload_to_aws(picture_path, CREDENTIALS.S3_BUCKET_NAME, f"{folder}/{picture_fn}")

    os.remove(picture_path)

    return picture_fn