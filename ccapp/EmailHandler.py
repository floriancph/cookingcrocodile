import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from flask import render_template
import ccapp.CREDENTIALS
import logging

def sendEmail(receiver_email, subject, body):
    sender_mail = ccapp.CREDENTIALS.EMAIL_ADDRESS
    password = ccapp.CREDENTIALS.EMAIL_PW

    # Create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = sender_mail
    message["To"] = receiver_email
    message["Subject"] = subject
    message.attach(MIMEText(body, "html"))

    server = smtplib.SMTP_SSL(ccapp.CREDENTIALS.EMAIL_SMTP, ccapp.CREDENTIALS.EMAIL_PORT)
    server.ehlo()
    server.login(ccapp.CREDENTIALS.EMAIL_USER, password)
    server.sendmail(sender_mail, receiver_email, message.as_string())
    server.quit()


def sendTokenEmail(receiver_email, token):
    logging.info("test")
    subject = "Zugangscode für Cooking Crocodile"
    body = render_template(
        "email_token.html",
        token=token)
    try:
        sendEmail(receiver_email, subject, body)
        logging.info(f"Email successfully sent to {receiver_email}")
    except Exception as e:
        logging.error(f"Failed to send email to {receiver_email}, {e}")