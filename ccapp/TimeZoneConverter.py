from datetime import datetime
from dateutil import tz

def convertFromUTCToLocal(time):
    from_zone = tz.tzutc()
    to_zone = tz.tzlocal()
    utc = time.replace(tzinfo=from_zone)
    local = utc.astimezone(to_zone)
    return local

def convertDatetimeToTime(time):
    return time.strftime("%H:%M")