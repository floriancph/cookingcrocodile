import time
from datetime import datetime
from ccapp.FormatManager import formattingTagsToDB, formattingTagsFromDB
from ccapp.DatabaseHandler import saveTagsToDatabase, getTagsFromDatabase
from ccapp.QRCodeGenerator import createQRCode
from ccapp import application as app
from ccapp.PictureSaver import save_picture
from ccapp import db, bcrypt
from flask import render_template, request, flash, json, redirect, url_for, jsonify
from ccapp.forms import LoginForm, CreateRecipeForm, UserPictureForm, ChangeRecipePicForm, ChangeProfileName, FeedbackForm, SearchForm
from ccapp.TokenGenerator import generateToken
from ccapp.EmailHandler import sendTokenEmail, sendEmail
from ccapp.models import User, Recipe, RecipeAndTag, RecipeAndLike, RecipeAndComment, DeletedPictures
from ccapp.TimeZoneConverter import convertFromUTCToLocal, convertDatetimeToTime
import ccapp.CREDENTIALS
from flask_login import login_user, current_user, logout_user
import operator


# FUNCTIONS ###

@app.route("/")
def homepage():
    return redirect(url_for("feed"))

@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("feed"))
    else:
        title = "Login"
        form = LoginForm()
        ### POST REQUEST ###
        if request.method == "POST" and form.validate_on_submit() and form.token.data != "":
            # If Email valid and Token was entered
            user = User.query.filter_by(email=form.email.data.strip().lower()).first() # get user from database (matching email)
            db_token_hashed = user.token # get hashed token 
            form_token = form.token.data.strip() # get token from form
            if bcrypt.check_password_hash(db_token_hashed, form_token):
                # if db token is equal to the token entered in the form
                login_user(user, remember=True)
                user.no_of_logins = user.no_of_logins + 1
                user.last_login = datetime.utcnow()
                user.device = request.headers.get('User-Agent')
                db.session.commit()
                flash("Zugangscode korrekt - Wilkommen zum Cooking Crocodile - Du bist jetzt eingeloggt", "success")
                return redirect(url_for("homepage"))
            else:
                flash("Falscher Zugangscode", "danger")
                return render_template("login.html", e_mail_correct=True, form=form, title=title)
        elif request.method == "POST" and form.validate_on_submit():
            token = generateToken(5)
            email = form.email.data.strip().lower() # get email from form
            app.logger.info(f'User {email} entered the email')
            name = email[:email.find("@")] # determine the name of the user (everything prior the @ sign)
            if len(name) > 15: # name should have max 15 characters
                name = name[:15]
            token_hashed = bcrypt.generate_password_hash(token).decode("utf-8") # create hash of the token
            flash(f"Zugangscode an {email} gesendet. Bitte dieses Fenster nicht schließen und Token einsetzen.", "success")
            if User.query.filter_by(email=email).first() == None: # if email is not yet in the database
                profile_hash = generateToken(30)
                qrcode = createQRCode(ccapp.CREDENTIALS.PROD_URL + "/user?id=" + profile_hash)
                user = User(email=email, token=token_hashed, name=name, profile_hash=profile_hash, qrcode=qrcode) # User neu anlegen
            else:
                user = User.query.filter_by(email=email).first() # Update User
                user.token = token_hashed
            db.session.add(user)
            db.session.commit()
            try:
                app.logger.info(f'Token: __{token}__')
                sendTokenEmail(email, token)
            except Exception as e:
                app.logger.error('Error sending Email, Token:', token, e)
            return render_template("login.html", e_mail_correct=True, form=form, title=title)
        elif request.method == "POST":
            flash(f"Incorrect Email Address: {form.email.data}", "danger")

        ### GET REQUEST ###
        return render_template("login.html", form=form, title=title)

def getRecipesFromTag(tag):
    tag = tag.strip().lower()
    query = RecipeAndTag.query.filter_by(tag=tag).all()
    result = []
    for item in query:
        recipe = Recipe.query.filter_by(recipe_id=item.recipe_id).first()
        result.append(recipe)
    return result

def getUserFromTag(tag):
    tag = tag.strip()
    result = User.query.filter_by(name=tag).all()
    # TODO CHECK, OB EMAIL SUCHERGEBNISSE HERBEIGEFÜHRT HAT
    return result

@app.route("/search", methods=["GET", "POST"])
def search():
    current_tags = RecipeAndTag.query.order_by(RecipeAndTag.id.desc()).limit(15).all()
    tag_dict = {}
    tag_list = []
    for tag in current_tags:
        if tag.tag in tag_dict:
            tag_dict[tag.tag] += 1
        else:
            tag_dict[tag.tag] = 1
            tag_list.append(tag.tag)
    form = SearchForm()
    if request.method == "POST":
        tag = form.search_field.data
    elif request.args.get("tag"):
        tag = request.args.get("tag")
    else:
        return render_template("search.html", current_tags=tag_list, page="search", form=form, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
    
    if tag:
        tag = tag.replace("#", "")
        result = getRecipesFromTag(tag)
        user_result = getUserFromTag(tag)
        noresults = True
        noresults_user = True
        total_no_results = False
        if result:
            noresults = False
        if user_result:
            noresults_user = False
        if not result and not user_result:
            total_no_results = True
        return render_template("search.html", total_no_results=total_no_results, noresults_user=noresults_user, noresults=noresults, search_tag=tag, current_tags=tag_list, page="search", form=form, result=result, user_result=user_result, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
    else:
        return render_template("search.html", current_tags=tag_list, page="search", form=form, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
    

@app.route("/user", methods=["GET", "POST"])
def user_profile():
    form = UserPictureForm()
    profile_name_form = ChangeProfileName()
    url_hash = request.args.get("id")
    user = User.query.filter_by(profile_hash=url_hash).first()
    recipes = user.recipes
    owner_is_viewer = False
    if current_user.is_authenticated:
        if current_user.profile_hash == url_hash:
            owner_is_viewer = True
            show_tooltips = False
            if current_user.showtooltips > 0:
                show_tooltips = True
                current_user.showtooltips = current_user.showtooltips - 1
                db.session.commit()
            if owner_is_viewer and request.method == "POST" and form.validate_on_submit():
                if form.picture.data:
                    picture_file = save_picture(form.picture.data, "profiles")
                    user.avatar = picture_file
                    db.session.commit()
                return render_template("userprofile.html", page="user", show_tooltips=show_tooltips, title=user.name, recipes=recipes, user=user, form=form, owner_is_viewer=owner_is_viewer, profile_name_form=profile_name_form, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
            else:
                return render_template("userprofile.html", page="user", show_tooltips=show_tooltips, title=user.name, recipes=recipes, user=user, form=form, owner_is_viewer=owner_is_viewer, profile_name_form=profile_name_form, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
    
    return render_template("userprofile.html", title=user.name, recipes=recipes, user=user, form=form, owner_is_viewer=owner_is_viewer, profile_name_form=profile_name_form, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)

@app.route("/feed")
def feed():
    # TOP TAGS / KATEGORIEN
    q_latest = Recipe.query.order_by(Recipe.id.desc()).limit(7).all()
    tags = {
        "Mittagessen": [0, "Mittagessen.jpg"], 
        "Abendessen": [0, "Abendessen.jpg"], 
        "Frühstück": [0, "Fruehstueck.jpg"], 
        "Vegetarisch": [0, "Vegetarisch.jpg"], 
        "Salat": [0, "Salat.jpg"], 
        "Dessert": [0, "Dessert.jpg"]
        }
    for tag in tags:
        q_tags = RecipeAndTag.query.filter_by(tag=tag.lower()).all()
        if q_tags:
            tags[tag][0] = len(q_tags)
    tags = list(tags.items())
    
    # FEED WITH LAST RECIPES
    for recipe in q_latest:
        recipe.recipe_created = convertFromUTCToLocal(recipe.recipe_created)
    accounts = User.query.order_by(User.id.desc()).limit(10).all()

    return render_template("feed.html", tags=tags, page="feed", title="Cooking Crocodile", s3bucket=ccapp.CREDENTIALS.S3_BUCKET, recipes=q_latest, accounts=accounts)

@app.route("/favorites")
def favorites():
    if current_user.is_authenticated:
        # Get favs
        favorites = RecipeAndLike.query.filter_by(user_id=current_user.id).all()
        recipes = []
        total_tags = {}
        # Get Recipes from favs
        for favorite in favorites:
            recipe = Recipe.query.filter_by(id=favorite.recipe_id).first()
            recipes.append(recipe)
            tags = RecipeAndTag.query.filter_by(recipe_id=recipe.recipe_id).all()
            for tag in tags:
                if tag.tag in total_tags:
                    total_tags[tag.tag] += 1
                else:
                    total_tags[tag.tag] = 1
        if len(total_tags) > 0:
            most_tag = max(total_tags.items(), key=operator.itemgetter(1))[0]
            most_tag = [most_tag, total_tags[most_tag]]
            most_tag_recipes = []
        else:
            most_tag = []
            most_tag_recipes = []

        for recipe in recipes:
            tags = RecipeAndTag.query.filter_by(recipe_id=recipe.recipe_id).all()
            tags_compare = []
            for tag in tags:
                tags_compare.append(tag.tag)
            if len(most_tag) > 0:
                if most_tag[0] in tags_compare:
                    most_tag_recipes.append(recipe)
        
        for recipe in current_user.recipes:
            tags = RecipeAndTag.query.filter_by(recipe_id=recipe.recipe_id).all()
            tags_compare = []
            for tag in tags:
                tags_compare.append(tag.tag)
            if len(most_tag) > 0:
                if most_tag[0] in tags_compare and recipe not in recipes:
                    most_tag_recipes.append(recipe)

        return render_template("favorites.html", most_tag=most_tag, most_tag_recipes=most_tag_recipes, page="fav", title="Favoriten", recipes=recipes, ownrecipes=current_user.recipes, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
    else:
        return redirect(url_for("login"))


@app.route("/newrecipe", methods=["GET", "POST"])
def new_recipe():
    if current_user.is_authenticated:
        form = CreateRecipeForm()
        if request.method == "POST":
            if form.validate_on_submit():
                if form.picture.data:
                    picture_file = save_picture(form.picture.data, "recipes")
                else:
                    picture_file = "default.jpg"
                recipe_id = generateToken(20)
                while Recipe.query.filter_by(recipe_id=recipe_id).first():
                    app.logger.info("Recipe Token already existing")
                    recipe_id = generateToken(20)
                title = form.title.data
                zutaten = form.zutaten.data
                zubereitung = form.zubereitung.data
                link = form.link.data.strip().lower()
                recipe = Recipe(
                    recipe_id=recipe_id, 
                    title=title, 
                    zutaten=zutaten, 
                    zubereitung=zubereitung, 
                    user_id=current_user.id,
                    recipe_pic=picture_file,
                    link=link)
                db.session.add(recipe)
                db.session.commit()

                #Speichere Tags separat in der Datenbank ab
                if form.tags.data:
                    tags = formattingTagsToDB(form.tags.data)
                    saveTagsToDatabase(tags, recipe_id)
                return redirect(url_for('view_recipe', recipe_id=recipe_id))
            else:
                return render_template("new_recipe.html", page="new", title="Neues Rezept", form=form, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
        else:
            return render_template("new_recipe.html", page="new", title="Neues Rezept", form=form, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
    else:
        return redirect(url_for("login"))

@app.route('/feedback', methods=['GET', 'POST'])
def feedback():
    if current_user.is_authenticated:
        form = FeedbackForm()
        if request.method == "GET":
            return render_template('feedback.html', form=form)
        elif request.method == "POST":
            feedback = form.feedback_text.data
            sendEmail(ccapp.CREDENTIALS.SUPPORT_EMAIL, 'USER FEEDBACK', feedback)
            flash("Vielen Dank für Dein Feedback", "success")
            return redirect(url_for('feed'))
    else:
        return redirect(url_for('login'))
        

@app.route("/viewrecipe", methods=["GET", "POST"])
def view_recipe():
    recipe_id = request.args.get("recipe_id")
    
    if recipe_id: # Falls Rezept vorhanden, zeige es an
        recipe = Recipe.query.filter_by(recipe_id=recipe_id).first()
        recipe.times_viewed = recipe.times_viewed + 1
        db.session.commit()
        recipe_owner_email = recipe.author.email
        recipe_tags = getTagsFromDatabase(recipe.recipe_id)
        recipe_liked = len(RecipeAndLike.query.filter_by(recipe_id=recipe.id).all())
        current_user_has_liked = False
        owner_is_viewer = False
        form = None
        show_tooltips = False
        if current_user.is_authenticated:
            form = ChangeRecipePicForm()
            if current_user.email == recipe_owner_email:
                owner_is_viewer = True
                if current_user.showtooltips > 0:
                    show_tooltips = True
                    current_user.showtooltips = current_user.showtooltips - 1
                    db.session.commit()
            if RecipeAndLike.query.filter_by(user_id=current_user.id, recipe_id=recipe.id).first():
                current_user_has_liked = True
            if owner_is_viewer and request.method == "POST" and form.validate_on_submit():
                if form.picture.data:
                    picture_file = save_picture(form.picture.data, "recipes")
                    recipe.recipe_pic = picture_file
                    db.session.commit()
        return render_template("view_recipe.html", show_tooltips=show_tooltips, form=form, current_user_has_liked=current_user_has_liked, recipe_tags=recipe_tags, title=recipe.title, recipe=recipe, owner_is_viewer=owner_is_viewer, recipe_liked=recipe_liked, s3bucket=ccapp.CREDENTIALS.S3_BUCKET)
    else:
        return redirect(url_for("homepage"))

@app.route("/logout")
def logout():
    logout_user()
    flash(f"Logout erfolgreich", "success")
    return redirect(url_for("homepage"))

# DELETE USER
@app.route("/delete")
def delete():
    user_id = current_user.id
    email = current_user.email
    logout_user()
    user = User.query.filter_by(id=user_id).first()
    delete_avatar = DeletedPictures(picture_id=user.avatar)
    db.session.add(delete_avatar)
    # Delete Likes from User
    user_likes = RecipeAndLike.query.filter_by(user_id=user_id).all()
    for like in user_likes:
        db.session.delete(like)
    db.session.commit()
    # Delete Recipes
    for recipe in user.recipes:
        # Delete likes from other users
        recipe_likes = RecipeAndLike.query.filter_by(recipe_id=recipe.id).all()
        for recipe_like in recipe_likes:
            db.session.delete(recipe_like)
        db.session.commit()
        # Delete Tags
        recipe_tags = RecipeAndTag.query.filter_by(recipe_id=recipe.recipe_id).all()
        for tag in recipe_tags:
            db.session.delete(tag)
        db.session.commit()
        # Finally delete recipe
        db.session.delete(recipe)
        db.session.commit()

    # Finally delete user
    db.session.delete(user)
    db.session.commit()
    app.logger.info(f"User {email} deleted")
    flash(f"Dein Account ({email}) wurde erfolgreich gelöscht", "success")
    return redirect(url_for("homepage"))

@app.route("/delete_tag", methods=["POST"])
def delete_tag():
    recipe_id = request.form['recipe_id']
    tag = request.form['tag']
    tag = tag.replace("#", "").lower()
    q = RecipeAndTag.query.filter_by(recipe_id=recipe_id, tag=tag).first()
    db.session.delete(q)
    db.session.commit()
    return jsonify({"status": "success"})


@app.route("/delete_recipe", methods=['POST'])
def delete_recipe():
    recipe_id = request.form['recipe_id']
    q = Recipe.query.filter_by(recipe_id=recipe_id).first()
    deleted_pic = DeletedPictures(picture_id=q.recipe_pic)
    # TODO DELETE COMMENTS FROM RECIPE
    db.session.add(deleted_pic)
    q3 = RecipeAndLike.query.filter_by(recipe_id=q.id).all()
    for like in q3:
        db.session.delete(like)
    q2 = RecipeAndTag.query.filter_by(recipe_id=recipe_id).all()
    for recipetag in q2:
        db.session.delete(recipetag)

    db.session.delete(q)
    db.session.commit()
    app.logger.info(f"Recipe {recipe_id} deleted")
    return jsonify({"status": "success"})


@app.route("/fav_recipe", methods=['POST'])
def fav_recipe():
    recipe_id = request.form['recipe_id']
    user_id = request.form['user_id']
    q = RecipeAndLike.query.filter_by(recipe_id=recipe_id, user_id=user_id).first()
    recipe = Recipe.query.filter_by(id=recipe_id).first()
    if q == None:
        new = RecipeAndLike(recipe_id=recipe_id, user_id=user_id)
        recipe.times_liked = len(RecipeAndLike.query.filter_by(recipe_id=recipe_id).all()) + 1
        db.session.add(new)
        db.session.commit()
    
    return jsonify({"status": "success"})


@app.route("/del_fav_recipe", methods=['POST'])
def del_fav_recipe():
    recipe_id = request.form['recipe_id']
    user_id = request.form['user_id']
    q = RecipeAndLike.query.filter_by(recipe_id=recipe_id, user_id=user_id).first()
    recipe = Recipe.query.filter_by(id=recipe_id).first()
    if q != None:
        recipe.times_liked = len(RecipeAndLike.query.filter_by(recipe_id=recipe_id).all()) - 1
        db.session.delete(q)
        db.session.commit()
    
    return jsonify({"status": "success"})

@app.route("/update_name", methods=['POST'])
def update_name():
    user_id = request.form['user_id']
    new_name = request.form['new_name']
    q = User.query.filter_by(id=user_id).first()
    q.name = new_name
    q.showtooltips = 0
    db.session.commit()
    return jsonify({"status": "success"})

###### UPDATING RECIPE ########

@app.route("/update_recipe", methods=['POST'])
def update_recipe():
    recipe_id = request.form['recipe_id']
    update_value = request.form['new_value']
    q = Recipe.query.filter_by(recipe_id=recipe_id).first()
    field = request.form['field']

    # TITLE
    if field == "title" and update_value != None:
        q.title = update_value
        q.author.showtooltips = 0
    elif field == "zutaten" and update_value != None:
        q.zutaten = update_value
    elif field == "zubereitung" and update_value != None:
        q.zubereitung = update_value
    elif field == "link" and update_value != None:
        q.link = update_value.strip().lower()
    elif field == "tag" and update_value != None:
        tag = formattingTagsToDB(update_value)
        saveTagsToDatabase(tag, recipe_id)
    
    db.session.commit()
    return "nothing"
    

@app.route('/sw.js', methods=['GET'])
def sw():
    return app.send_static_file('sw.js')

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html', title="404", s3bucket=ccapp.CREDENTIALS.S3_BUCKET), 404

@app.errorhandler(500)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('500.html', title="500", s3bucket=ccapp.CREDENTIALS.S3_BUCKET), 500

@app.route('/test')
def test():
    return render_template('test.html')