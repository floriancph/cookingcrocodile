from datetime import datetime
from ccapp import db, login_manager
from flask_login import UserMixin

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    profile_hash = db.Column(db.Text)
    email = db.Column(db.String(120), unique=True)
    token = db.Column(db.Text)
    name = db.Column(db.Text)
    avatar = db.Column(db.Text, default="default_avatar.png")
    user_description = db.Column(db.Text)
    no_of_logins = db.Column(db.Integer, default=0)
    first_login = db.Column(db.DateTime, default=datetime.utcnow)
    last_login = db.Column(db.DateTime)
    recipes = db.relationship("Recipe", backref="author", lazy=True)
    qrcode = db.Column(db.Text)
    showtooltips = db.Column(db.Integer, default=5) #Number of shown tooltips
    birthday = db.Column(db.Text)
    device = db.Column(db.Text)

    def __repr__(self):
        return f"User('{self.id}', '{self.email}', '{self.name}')"


class Recipe(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    recipe_id = db.Column(db.Text, unique=True)
    title = db.Column(db.Text)
    zutaten = db.Column(db.Text)
    zubereitung = db.Column(db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    link = db.Column(db.Text)
    recipe_pic = db.Column(db.Text)
    tags = db.Column(db.Text)
    times_shared = db.Column(db.Integer, default=0)
    times_viewed = db.Column(db.Integer, default=0)
    times_cooked = db.Column(db.Integer, default=0)
    times_liked = db.Column(db.Integer, default=0)
    last_time_viewed = db.Column(db.DateTime, default=datetime.utcnow)
    recipe_created = db.Column(db.DateTime, default=datetime.utcnow)
    qrcode = db.Column(db.Text)

    def __repr__(self):
        return f"Recipe('{self.recipe_id}', '{self.title}')"

class RecipeAndTag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag = db.Column(db.Text)
    recipe_id = db.Column(db.Text)

class RecipeAndLike(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe.id'), nullable=False)

class RecipeAndComment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    recipe_id = db.Column(db.Integer, db.ForeignKey('recipe.id'), nullable=False)
    comment = db.Column(db.Text)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    liked = db.Column(db.Integer)

class DeletedPictures(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    picture_id = db.Column(db.Text)