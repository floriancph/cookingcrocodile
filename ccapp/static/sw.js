/*'use strict';

var cacheVersion = 1;
var currentCache = {
  offline: 'ccapp-offline-cache' + cacheVersion
};
const offlineUrl = '/static/offline2.html';

this.addEventListener('install', event => {
  event.waitUntil(
    caches.open(currentCache.offline).then(function(cache) {
      return cache.addAll([
          offlineUrl
      ]);
    })
  );
});

this.addEventListener('fetch', event => {
  // request.mode = navigate isn't supported in all browsers
  // so include a check for Accept: text/html header.
  if (event.request.mode === 'navigate' || (event.request.method === 'GET' && event.request.headers.get('accept').includes('text/html'))) {
        event.respondWith(
          fetch(event.request.url).catch(error => {
              // Return the offline page
              return caches.match(offlineUrl);
          })
    );
  }
  else{
        // Respond with everything else if we can
        event.respondWith(caches.match(event.request)
                        .then(function (response) {
                        return response || fetch(event.request);
                    })
            );
      }
});
*/

var CACHE_NAME = 'offline-ccapp';
var urlsToCache = [
  '/',
  '/feed',
  '/login',
  '/favorites',
  '/static/js/script.js',
  '/static/js/jquery-3.4.1.min.js',
  '/static/js/form.js',
  '/static/css/owncss.css',
  'https://use.fontawesome.com/releases/v5.8.2/css/all.css',
  'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
  'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
  'https://cdnjs.cloudflare.com/ajax/libs/pulltorefreshjs/0.1.17/index.umd.min.js',
  '/static/css/bootstrap.min.css',
  '/static/MDB/css/mdb.min.css',
  '/static/icons/croc_icon.png',
  '/static/icons/discover.png',
  '/static/images/bg_cook.jpg',
  '/static/offline3.html'
];
self.addEventListener('install', function(event) {
  // install files needed offline
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
    if (navigator.onLine) {
      event.respondWith(
        fetch(event.request).catch(function() {
          return caches.match(event.request);
        })
      );
    } else {
      event.respondWith(
        fetch(event.request.url).catch(error => {
            // Return the offline page
            return caches.match('/static/offline3.html');
        })
      );
    }
    
});