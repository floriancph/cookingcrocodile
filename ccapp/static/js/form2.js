$(document).ready(function() {

    // CHANGE RECPE TITLE
    //$('#recipetitle').tooltip('show');
    $('[data-toggle="tooltip"]').tooltip('show')
    $('#recipetitle').on('click', function() {
        try {
            var title = document.getElementById('recipetitle').textContent;
            var edit_title = document.getElementById('edit_title');
            var edit_title_class = document.getElementById('edit_title_class');
            edit_title.value = title;
            edit_title_class.style.display = "block";
            document.getElementById('recipetitle').style.display = "none";
            edit_title.focus();
        } catch (e) {
            console.log("No valid user")
        }
    });

    $('#edit_title').focusout(function() {
        
        var old_title = document.getElementById('recipetitle');
        var new_title = document.getElementById('edit_title').value;
        var edit_title_class = document.getElementById('edit_title_class');
        var recipe_id = edit_title_class.getAttribute('recipe_id');
        var difference = old_title.textContent != new_title;
        var valid_title = new_title.trim() != ""

        if (valid_title) {
            old_title.textContent = new_title;
        }
        
        edit_title_class.style.display = "none";
        old_title.style.display = "block";

        if (valid_title && difference) {
            // save new title in database
            req = $.ajax({
                data : {
                    recipe_id : recipe_id,
                    field : 'title',
                    new_value : new_title
                },
                type : 'POST',
                url : '/update_recipe'
            });
        }
    });

    // CHANGE RECIPE ZUTATEN
    $('#zutatendiv').on('click', function() {
        try {
            var zutaten = document.getElementById('zutaten_field').innerHTML;
            if (zutaten == "Keine Zutaten eingetragen") {
                zutaten = "";
            }
            var edit_zutaten = document.getElementById('edit_zutaten_field');
            var edit_zutaten_class = document.getElementById('edit_zutaten_class');
            edit_zutaten.value = zutaten;
            edit_zutaten_class.style.display = "block";
            document.getElementById('zutatendiv').style.display = "none";
            edit_zutaten.focus();
        } catch (e) {
            console.log("No valid user")
        }
        
    });

    $('#edit_zutaten_field').focusout(function() {
        
        var old_zutaten = document.getElementById('zutaten_field');
        var new_zutaten = document.getElementById('edit_zutaten_field').value;
        var edit_zutaten_class = document.getElementById('edit_zutaten_class');
        var recipe_id = edit_zutaten_class.getAttribute('recipe_id');

        old_zutaten.innerHTML = new_zutaten;
        if (new_zutaten.trim() == "") {
            old_zutaten.innerHTML = "Keine Zutaten eingetragen"
        }
        edit_zutaten_class.style.display = "none";
        document.getElementById('zutatendiv').style.display = "block";

        // save new zutaten in database
        req = $.ajax({
            data : {
                recipe_id : recipe_id,
                field : 'zutaten',
                new_value : new_zutaten
            },
            type : 'POST',
            url : '/update_recipe'
        });
    });


    // CHANGE RECIPE ZUBEREITUNG
    $('#zubereitungdiv').on('click', function() {
        try {
            var zubereitung = document.getElementById('zubereitung_field').innerHTML;
            if (zubereitung == "Keine Zubereitung eingetragen") {
                zubereitung = "";
            }
            var edit_zubereitung = document.getElementById('edit_zubereitung_field');
            var edit_zubereitung_class = document.getElementById('edit_zubereitung_class');
            edit_zubereitung.value = zubereitung;
            edit_zubereitung_class.style.display = "block";
            document.getElementById('zubereitungdiv').style.display = "none";
            edit_zubereitung.focus();
        } catch (e) {
            console.log("No valid user")
        }
        
    });

    $('#edit_zubereitung_field').focusout(function() {
        
        var old_zubereitung = document.getElementById('zubereitung_field');
        var new_zubereitung = document.getElementById('edit_zubereitung_field').value;
        var edit_zubereitung_class = document.getElementById('edit_zubereitung_class');
        var recipe_id = edit_zubereitung_class.getAttribute('recipe_id');

        old_zubereitung.innerHTML = new_zubereitung;
        if (new_zubereitung.trim() == "") {
            old_zubereitung.innerHTML = "Keine Zubereitung eingetragen"
        }
        edit_zubereitung_class.style.display = "none";
        document.getElementById('zubereitungdiv').style.display = "block";

        // save new zutaten in database
        req = $.ajax({
            data : {
                recipe_id : recipe_id,
                field : 'zubereitung',
                new_value : new_zubereitung
            },
            type : 'POST',
            url : '/update_recipe'
        });
    });


    $('#editlinkbutton').on('click', function() {
        var recipe_id = document.getElementById('editlinkbutton').getAttribute('recipe_id');
        var curr_link = document.getElementById('editlinkbutton').getAttribute('curlink');
        var new_link = prompt("Link: ", curr_link);
        var linkbutton = document.getElementById('linkbutton');
        var link_for_link_button = document.getElementById('link_for_linkbutton');
        if (new_link == null) {
            new_link = curr_link;
        } else if (new_link.trim() != "") {
            linkbutton.textContent = "Link öffnen";
            link_for_link_button.setAttribute("href", new_link);
            link_for_link_button.setAttribute("target", "_blank");
            linkbutton.removeAttribute("disabled");
            document.getElementById('editlinkbutton').setAttribute('curlink', new_link.trim());
        } else {
            linkbutton.textContent = "Kein Link";
            link_for_link_button.setAttribute("disabled", "True");
            link_for_link_button.setAttribute("href", "#");
            link_for_link_button.removeAttribute("target");
            linkbutton.setAttribute("disabled", "True");
            document.getElementById('editlinkbutton').setAttribute('curlink', "");
        }

        // save new link in database
        req = $.ajax({
            data : {
                recipe_id : recipe_id,
                field : 'link',
                new_value : new_link
            },
            type : 'POST',
            url : '/update_recipe'
        });

    });

    // ADD TAG
    $('#addtag').on('click', function() {
        var recipe_id = document.getElementById('addtag').getAttribute('recipe_id');
        var new_tag = prompt("Neuer Tag: ", "#");

        if (new_tag != "" && new_tag != null) {
            var span = document.createElement("button");
            span.className = "btn badge badge-pill purple-gradient ownmediumtext mb-1 mr-1 shadow textoverflow";
            span.style = "max-width: 100%; word-wrap: break-word;";
            var node = document.createTextNode(new_tag);
            span.appendChild(node);
            var element = document.getElementById("tags");
            element.appendChild(span);
        }

        // save new tag to database
        req = $.ajax({
            data : {
                recipe_id : recipe_id,
                field : 'tag',
                new_value : new_tag
            },
            type : 'POST',
            url : '/update_recipe'
        });

    });



    $('.deleterecipe').on('click', function() {
        
        var recipe_id = $(this).attr('recipe_id');
        document.getElementById("ansehen"+recipe_id).style.transition = "all 1s";
        document.getElementById("ansehen"+recipe_id).className = "btn btn-light disabled";

        // DATA SENDING TO BACKEND
        req = $.ajax({
			data : {
                recipe_id : recipe_id
			},
			type : 'POST',
			url : '/delete_recipe'
        });
        
        // RECEIVING DATA FROM BACKEND
        req.done(function(data) {
            // var item = document.getElementById("listitem"+stock_symbol);
            if (data.status == 'success') {
                document.getElementById("buttondelete"+recipe_id).style.transition = "all 1s";
                document.getElementById("icondelete"+recipe_id).style.transition = "all 1s";
                document.getElementById("icondelete"+recipe_id).className = "fas fa-check-square";
                document.getElementById("buttondelete"+recipe_id).className = "btn btn-light px-3";
                document.getElementById("buttondelete"+recipe_id).setAttribute("disabled", "True");
                //$('#buttondelete'+stock_symbol).text('deleted');
            } else {
                alert('Failed to delete');
            }

        });
    
    });

    $(".deleteontag").on('click', function(){
        var tag = $(this).attr('tag');
        var recipe_id = $(this).attr('recipe_id');
        var lindex = $(this).attr('lindex');
        var pill = document.getElementById("tagpill"+lindex);
        var deletebtn = document.getElementById('tagdelete'+lindex);
        var owner_is_viewer = $(this).attr('owner_is_viewer');

        if (owner_is_viewer == "True") {
            pill.transition = "all 2s";
            pill.className = "btn badge badge-pill badge-danger ownmediumtext mb-1 shadow textoverflow tagpill";
            pill.style.setProperty("text-decoration", "line-through");
            deletebtn.style.display = "none";
            // DATA SENDING TO BACKEND
            req = $.ajax({
                data : {
                    recipe_id : recipe_id,
                    tag : tag
                },
                type : 'POST',
                url : '/delete_tag'
            });
        }
    });


    // REZEPTE MERKEN
    $('#rezeptmerken').on('click', function() {
        var is_authenticated = $(this).attr('user_is_authenticated');
        var current_user_has_liked = $(this).attr('current_user_has_liked');
        var recipe_id = $(this).attr('recipe_id'); 
        var user_id = $(this).attr('user_id');

        if (is_authenticated == "True") {
            var button_merken = document.getElementById("rezeptmerken")
            var recipe_liked = $(this).attr('no_of_likes');
            if (current_user_has_liked == "False") {
                var recipe_liked = $(this).attr('no_of_likes');
                recipe_liked = parseInt(recipe_liked) + 1;
                button_merken.setAttribute("current_user_has_liked", "True");
                button_merken.setAttribute("no_of_likes", recipe_liked);
                button_merken.style = "background-color: #42d62f;";
                button_merken.innerHTML = recipe_liked + ' <i class="fas fa-heart"></i>';
                // DATA SENDING TO BACKEND
                req = $.ajax({
                    data : {
                        recipe_id : recipe_id,
                        user_id : user_id
                    },
                    type : 'POST',
                    url : '/fav_recipe'
                });
            } else {
                var recipe_liked = $(this).attr('no_of_likes');
                recipe_liked = parseInt(recipe_liked) - 1;
                button_merken.setAttribute("current_user_has_liked", "False");
                button_merken.setAttribute("no_of_likes", recipe_liked);
                button_merken.style = "";
                button_merken.innerHTML = recipe_liked + ' <i class="fas fa-heart"></i>';
                // DATA SENDING TO BACKEND
                req = $.ajax({
                    data : {
                        recipe_id : recipe_id,
                        user_id : user_id
                    },
                    type : 'POST',
                    url : '/del_fav_recipe'
                });
            }
            
        }
    
    });


    // CHANGE USER NAME
    $('#username').on('click', function() {
        try {
            var title = document.getElementById('username').textContent;
            var edit_title = document.getElementById('name');
            var edit_title_class = document.getElementById('edit_name_class');
            edit_title.value = title;
            edit_title_class.style.display = "block";
            document.getElementById('username').style.display = "none";
            edit_title.focus();
        } catch (e) {
            console.log("No valid user")
        }
    });

    $('#name').focusout(function() {
        
        var old_title = document.getElementById('username');
        var new_title = document.getElementById('name').value;
        var edit_title_class = document.getElementById('edit_name_class');
        var user_id = edit_title_class.getAttribute('user_id');
        var difference = old_title.textContent != new_title;
        var valid_title = new_title.trim() != ""

        if (valid_title) {
            old_title.textContent = new_title.trim();
        }
        
        edit_title_class.style.display = "none";
        old_title.style.display = "block";

        if (valid_title && difference) {
            // save new title in database
            req = $.ajax({
                data : {
                    user_id : user_id,
                    new_name : new_title.trim()
                },
                type : 'POST',
                url : '/update_name'
            });
        }
    });

    // Delete Account - Spinner
    $('#deleteaccount').on('click', function() {
        document.getElementById("deleteaccount").className = "btn btn-danger btn-lg text-white";
        document.getElementById("deleteaccount").innerHTML = '<div class="loader" align="center"></div>';
    });

    // Create new Recipe - Spinner / New Profile Picture / New Recipe Picture Spinner
    $('#speichern').on('click', function() {
        document.getElementById("loader").style.display = 'block';
        document.getElementById("speichern").style.display = 'none';
    });

    $('#sharerecipe').on('click', function() {
        var text = 'Ich habe ein Rezept für Dich';
        var recipe_id = document.getElementById('sharerecipe').getAttribute('recipe_id');
        var text_url = location.href;
        location.href = 'https://api.whatsapp.com/send?text=' + encodeURIComponent(text + ' - ') + text_url;
    });

    PullToRefresh.init({
      mainElement: '.scrollarea',
        triggerElement: '.refresh',
        onRefresh: function (cb) {
            document.getElementById("refresher").style.display = "block";
            document.getElementById("arrowdown").style.display = "none";
           setTimeout(function () {
              cb();
              location.reload();
            }, 1500);
        }
    });

    $('#profilepic').on('click', function() {
        document.getElementById('overlay').style.display = "block";
    });

    $('.overlayimage').on('click', function() {
        document.getElementById('overlay').style.display = "none";
    });

    // Login - Spinner
    $('#anmbtn').on('click', function() {
        document.getElementById("loader").style.display = 'block';
        document.getElementById("anmbtn").style.display = 'none';
    });

    // Send Search
    $('#searchbtn').on('click', function() {
        document.getElementById("loader").style.display = 'block';
        document.getElementById("searchbtn").style.display = 'none';
    });

    // Send Search
    $('.searchtagpill').on('click', function() {
        var tag = $(this).attr('tag');
        document.getElementById("search_field").value = tag;
    });

});

function submitButtonStyle(_this) {
    _this.style.transition = "all 2s";
    _this.innerText = "Bild ausgewählt";
    if (_this.getAttribute("disappear") == "True") {
        _this.style.display = "none";
    }
    var speichernbutton = document.getElementById("speichern");
    speichernbutton.style.display = "block";
  }