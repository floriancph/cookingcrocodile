import pyqrcode, png, os
from ccapp import application as app
from ccapp.TokenGenerator import generateToken
from ccapp.PictureSaver import upload_to_aws
import ccapp.CREDENTIALS as CREDENTIALS

def createQRCode(text):
    filename = generateToken(30) + ".png"
    path = os.path.join(app.root_path, "static", "qrcodes", filename)
    code = pyqrcode.create(text)
    code.png(path, scale=5, module_color=(0x0, 0x0, 0x0),background=(0xff, 0xff, 0xff, 0xBB))
    upload_to_aws(path, CREDENTIALS.S3_BUCKET_NAME, f"qrcodes/{filename}")
    os.remove(path)
    return filename
