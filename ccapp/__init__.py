from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from ccapp import CREDENTIALS
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
import logging

application = Flask(__name__)
logging.basicConfig(filename='logging.log', level=logging.DEBUG, format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')
application.config["SECRET_KEY"] = CREDENTIALS.APP_SECRET_KEY
application.config["SQLALCHEMY_DATABASE_URI"] = CREDENTIALS.DATABASE_SQLALCHEMY_URI_TEST
db = SQLAlchemy(application)
bcrypt = Bcrypt(application)
login_manager = LoginManager(application)

from ccapp import routes