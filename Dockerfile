FROM python:3.9
LABEL MAINTAINER floriancph
COPY . /appdir
WORKDIR /appdir
RUN pip install -r requirements.txt
EXPOSE 5000
CMD ["python", "application.py"]