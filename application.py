from ccapp import application
import sys

if __name__ == "__main__":
    # Run app
    if len(sys.argv) > 1:
        if sys.argv[1] == '--debug':
            application.run(host='0.0.0.0', debug=True)
    else:
        application.run(host='0.0.0.0', debug=False)