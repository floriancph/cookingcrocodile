# About this Software
Everyone who is into cooking loves to explore new recipes and share them with friends & family. This software was designed to fulfill that need and to give a possibility to use a website which is compatible with various devices. Collecting & sharing recipes is easily possible and results in an interactive platform for everyone.

This website was built with a new paradigm of login-mechanism: None of the users have to register at the website - users just have to enter their email, regardless if the user was on the website before or not, and the website sends a token to the respective email address. After confirmation of the token the user is either registered as new user or just logged in and has all the recipes available he/she has entered before. No annoying password to remember, just login with the token and repeat this for various devices.

# Initial Setup Instructions for the Project (starting from zero)

- clone this repository
- create a ```CREDENTIALS.py``` file from the ```CREDENTIALS_TEMPLATE.py``` template and save it to the /ccapp directory
- create a POSTGRESQL database and enter the details into the ```CREDENTIALS.py``` file
- create an S3 Bucket in AWS, enter the details into the ```CREDENTIALS.py``` file and make it publicly available
- create three directories in the S3 Bucket: 'recipes', 'qrcodes', 'profiles'
- upload default pictures for the 'recipes' and 'profiles' directory (so in case that the user does not place an image there is at least a default picture):
    - 'profiles/default_avatar.png'
    - 'recipes/default.jpg'
- create an email (GMAIL) account from which you want to send users the access token and enter the details into the ```CREDENTIALS.py``` file
- fill rest of the needed credentials into the ```CREDENTIALS.py``` file

- create a python virtual environment with ```python3 -m venv env/```
- activate the virtual environment with ```source env/bin/activate```
- run the command ```python --version``` to check whether the correct python version was chosen
- run the command ```pip install pip --upgrade``` to upgrade pip
- run the command ```pip install -r requirements.txt``` to install all the packages required to run the project

- run the ```clear_db.py``` file with ```python clear_db.py``` to initiate the database and its tables (CAUTION: If your database already contains tables, they will all be deleted after running the script). The data model can be viewed at ```models.py```

- run the command ```python application.py --debug``` to start the server in debug-mode
- run the command ```python application.py``` to start the server in production-mode

# Create Docker Container 

- Build Docker Image: ```docker build -t cookingcrocodile .```
- Run Docker Container on Port 5000: ```docker run -d --name cookingcrocodile -p 5000:5000 cookingcrocodile```
- Upload it into a Container Registry at your choice and run it on any cluster you want...

# Debugging & Logging

- The application makes use of basic logging functionalities in flask. Access the ```logging.log``` file
- Access the the command line of the docker image with ```docker exec -it <container id> bash```
- Open the ```logging.log``` file with the command ```cat logging.log``` and receive information about latest logs
- Sometimes the Gmail client does not work due to security restrictions. Visit [this](https://support.google.com/mail/answer/7126229?visit_id=637519318893194365-1249371859&rd=2#cantsignin) website to find help
