from ccapp import db
u = input("Are you sure to delete the existing database tables? (Y/n) ")
if u == 'Y':
    db.drop_all()
    print("All tables dropped")
    db.create_all()
    print("All tables created")
else:
    print("process canceled")